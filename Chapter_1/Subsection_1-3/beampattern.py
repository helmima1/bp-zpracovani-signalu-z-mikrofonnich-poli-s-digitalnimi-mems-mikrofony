import sys
import os
import numpy as np
import matplotlib
matplotlib.use('GTK3Agg')
import matplotlib.pyplot as plt


current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
FREQUENCIES = [750, 2700, 4200]  # Frequencies used to calculate 3 graphs at the time
C = 343
MIC_DIST = 0.1
RESOLUTION = 2000

# --------------- BEAMPATTERNS CALCULATION ---------------
beampatterns = []

for freq in FREQUENCIES:
    beampattern_cardioid = cf.cardioid_beampattern(freq, MIC_DIST, RESOLUTION, c=C)
    amplitudes_cardioid = cf.signal_to_decibels(np.abs(beampattern_cardioid))

    beampatterns.append(amplitudes_cardioid)

# --------------- DISPLAYING PLOTS ---------------

fig, axes = plt.subplots(ncols=3, subplot_kw={'projection': 'polar'}, figsize=(10.3, 3))
reference_radians_values = np.linspace(0, 2 * np.pi, RESOLUTION)
for cols in range(0, 3):
    axes[cols].plot(reference_radians_values, beampatterns[cols], color='blue', linewidth='1.2')
    axes[cols].set_ylim([-40, 0])
    axes[cols].set_title(f"{FREQUENCIES[cols]} Hz", va='bottom')
    box = axes[cols].get_position()
    axes[cols].set_position([box.x0, box.y0, box.width * 0.9, box.height * 0.9])
    axes[cols].set_yticks([-40, -30, -20, -10, 0, 5])

plt.show()