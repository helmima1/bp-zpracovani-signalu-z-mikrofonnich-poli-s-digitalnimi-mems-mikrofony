import os
import sys
import numpy as np
import matplotlib.pyplot as plt

# Change directories to import ComputationFunctions
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
FREQUENCY = 2000
C = 343
PHI = 0
THETA = 90
M = 8
RADIUS = 0.05 # in meters
resolution = 1000

# --------------- BEAMPATTERN CALCULATION ---------------

beampattern = cf.cma_beampattern(THETA, PHI, FREQUENCY, M, RADIUS, C, resolution)
amplitudes = cf.signal_to_decibels(np.abs(beampattern))

# --------------- DISPLAY PLOT ---------------

fig, axes = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(3, 3), dpi=200)
reference_radians_values = np.linspace(0, 2 * np.pi, resolution)
axes.plot(reference_radians_values, amplitudes)
axes.set_ylim([-40, 0])
axes.set_yticks([-40, -30, -20, -10, 0])

plt.show()