"""
Source code for all essential functions used along the thesis are defined in this python file. This is to avoid
copying code used in other scripts.
"""
import numpy as np
from enum import Enum
from scipy.signal import butter, freqz

# ----------------------------------------------------------------
# ------------------------- ENUMERATIONS -------------------------
# ----------------------------------------------------------------

class RMSfilter(Enum):
    """Definition of filter regimes for search of the loudest source finder by cardioid and dsb algorithm.
    """
    LOWPASS_ONLY = 1
    HIGHPASS_ONLY = 2,
    BOTH = 3

class AppRegime(Enum):
    """Definition of algorithm that is being used in current run of the script. 
    """
    DSB_BASIC = 1,
    DSB_ADVANCED = 2,
    CARDIOID_DSB_ADVANCED =  3

class FLSRegime(Enum):
    """Definition of filter regimes for search of the loudest source finder by cardioid and dsb algorithm.
    """
    DSB = 1
    DSB_CARD_INTERPOL = 2
    DSB_CARD_BUTTER = 3

# ----------------------------------------------------------------
# ---------------------- ANALYSIS FUNCTIONS ----------------------
# ----------------------------------------------------------------

def degrees_to_radians(degrees):
    """Converts degrees to radians.
    
    :param degrees: Angle in degrees.
    :type degrees: int
    :return: Angle in radians.
    :rtype: float
    """
    return degrees * (np.pi / 180)


def signal_to_decibels(signal: np.ndarray, reference: int = 1) -> np.ndarray:
    """Converts signal to decibels with respect to the reference value. If no reference value is given, it is set to 1.
        
    :param signal: Signal to be converted to decibels
    :type signal: np.ndarray
    :param reference: Reference value for decibels
    :type reference: int
    :return: Signal converted to decibels
    :rtype: np.ndarray
    """
    decibels = 10 * np.log10(signal / reference)
    return decibels

def quantize_tmi(latencies, sampling_frequency):
    """
    Takes an array of latencies as an input and returns them back quantized to a given sampling frequency.
    Most common sampling frequencies are 44.1 kHz, 48 kHz and 96 kHz.

    :param latencies: input Latencies ment to be quantized.
    :type latencies: np.ndarray
    :return: Array of latencies quantized to certain sampling frequency.
    :rtype: np.ndarray        
    """
    minimum_latency = (1 / sampling_frequency)
    result = []
    for i in latencies:
        result.append(round(i / minimum_latency)*minimum_latency)
    return result

def quantize_tmi_to_samples(latencies: np.ndarray, sampling_frequency: int) -> np.ndarray:
    """
    Takes an array of latencies as an input and returns them back quantized to a given sampling frequency.
    Most common sampling frequencies are 44.1 kHz, 48 kHz and 96 kHz.

    :param latencies: input Latencies ment to be quantized.
    :type latencies: np.ndarray
    :return: Array of latencies quantized to certain sampling frequency.
    :rtype: np.ndarray        
    """
    minimum_latency = (1 / sampling_frequency)
    result = np.round(latencies/minimum_latency)
    return result

def cma_tmi(radius: float, M: int, phi_angle: float, theta_angle: float, c: int) -> np.ndarray:
    """
    Calculates delay times on all microphones in circular microphone array. Results are stored and returned in a list,
    which indices are matching indices of each microphone.
    :param radius: radius of circular microphone array
    :type radius: float
    :param M: number of microphones in circular microphone array
    :type M: int
    :param phi_angle: horizontal angle of a sound source
    :type phi_angle: float
    :param theta_angle: vertical angle of a sound source
    :type theta_angle: float
    :param c: speed of sound
    :type c: int
    :return: list of delays where every index of the list corresponds to the microphone index delay
    :rtype: np.ndarray
    """
    latencies = np.empty(M)
    k_hat_vector = np.array([
        np.sin(degrees_to_radians(theta_angle)) * np.cos(degrees_to_radians(phi_angle)),
        np.sin(degrees_to_radians(theta_angle)) * np.sin(degrees_to_radians(phi_angle)),
        np.cos(degrees_to_radians(theta_angle))
    ])
    # Dot product for k_hat and r_m_i for all mics
    for mic_id in range(M):
        rm_vector = radius * np.array([
            np.cos(2 * np.pi * (mic_id / M)),
            np.sin(2 * np.pi * (mic_id / M)),
            0
        ])
        t_m_i = np.dot(k_hat_vector, rm_vector)/c
        latencies[mic_id] = t_m_i
    # Find smallest t_m_i and adds it to the latencies according to fomrula 1.2
    t_m_c = np.max(latencies)
    latencies = t_m_c - latencies
    return latencies

def cma_beampattern(theta: float, phi: float, frequency: int, M: int, radius: float, c: int, resolution: int) -> list:
    """
    This function calculates beam pattern of circular microphone array. Beampattern is calculated in frequency domain,
    therefore frequency as a parameter is required. Returned data are best displayed on polar plot.

    :param theta: vertical angle of a sound source
    :type theta: float
    :param phi: horizontal angle of a sound source
    :type phi: float
    :param frequency: frequency of a beam pattern
    :type frequency: int
    :param M: number of microphones in circular microphone array
    :type M: int
    :param radius: radius of circular microphone array
    :type radius: float
    :param c: speed of sound
    :type c: int
    :param resolution: number of points that should be generated in range of 0..360 degrees
    :type resolution: int
    :return: list of all magnitudes of the signals dispersed in directions of 0.360 degrees
    """    
    ro = 2 * np.pi * frequency
    beampattern = []
    tmcmi = cma_tmi(radius, M, phi, theta, c)
    degrees_resolution = np.linspace(0, 360, resolution)
    # Calculate H_DSB according to formula 1.4
    for phi_s in degrees_resolution:
        kj = cma_tmi(radius, M, phi_s, theta, c)
        sum = 0
        for index in range(0, M):
            sum += np.exp(1j * ro * (tmcmi[index] - kj[index]))
        beampattern.append(sum / M)
    return beampattern

def cma_beampattern_quantization(theta: float, phi: float, frequency: int, M: int, radius: float, c: int, resolution: int, sf: int) -> list:
    """
    This function calculates beam pattern of circular microphone array. Beampattern is calculated in frequency domain,
    therefore frequency as a parameter is required. Returned data are best displayed on polar plot. This function calculates
    with quantized delays of the microphones.

    :param theta: vertical angle of a sound source
    :type theta: float
    :param phi: horizontal angle of a sound source
    :type phi: float
    :param frequency: frequency of a beam pattern
    :type frequency: int
    :param M: number of microphones in circular microphone array
    :type M: int
    :param radius: radius of circular microphone array
    :type radius: float
    :param c: speed of sound
    :type c: int
    :param resolution: number of points that should be generated in range of 0..360 degrees
    :type resolution: int
    :return: list of all magnitudes of the signals dispersed in directions of 0.360 degrees with applied quantization.
    :rtype: list
    """
    ro = 2 * np.pi * frequency
    result = []
    tmcmi = quantize_tmi(cma_tmi(radius, M, phi, theta, c), sf)
    degrees_resolution = np.linspace(0, 360, resolution)
    # Calculate H_DSB according to formula 1.4
    for phi_s in degrees_resolution:
        kj = cma_tmi(radius, M, phi_s, theta, c)
        sum = 0
        for index in range(0, M):
            sum += np.exp(1j * ro * (tmcmi[index] - kj[index]))
        result.append(sum / M)
    return result

def cardioid_beampattern(freq: int, mic_dist: float, resolution: int, tau: float = None, c=343) -> np.ndarray:
    """This function calculates beampattern of cardioid microphone array. Beampattern is calculated in frequency domain,
    therefore frequency as a parameter is required. Returned data are best displayed on polar plot.

    :param freq: frequency of a beam pattern
    :type freq: int
    :param mic_dist: distance between two microphones
    :type mic_dist: float
    :param resolution: number of points that should be generated in range of 0..360 degrees
    :type resolution: int
    :param tau: delay between two microphones, defaults to None
    :type tau: float, optional
    :param c: speed of sound in m/s, defaults to 343
    :type c: int, optional
    :return: list of all magnitudes of the signals dispersed in directions of 0.360 degrees
    :rtype: np.ndarray
    """
    degrees_resolution = np.linspace(0, 2*np.pi, resolution)
    if tau == None:
        tau = mic_dist / c # mic_dist must be given in meters!
    ro = 2 * np.pi * freq

    beampattern = ((1 - np.exp(-1j * ro * ((mic_dist/c) * np.cos(degrees_resolution) + tau)))
                   /
                   (1 - np.exp(-1j * ro * ((mic_dist/c) + tau))))
    return beampattern

def cardioid_tf(phi: int, mic_dist: float, max_frequency=1000, c=343) -> np.ndarray:
    """Calculates transfer function for cardioid microphone array. Outuput is best used for plotting in polar plot.

    :param phi: angle of a sound source
    :type phi: int
    :param mic_dist: distance between two microphones
    :type mic_dist: float
    :param max_frequency: maximum frequency at which tf should be calculated, defaults to 1000
    :type max_frequency: int, optional
    :param c: speed of sound in m/s, defaults to 343
    :type c: int, optional
    :return: transfer function of cardioid with quantized delays
    :rtype: np.ndarray
    """
    frequencies = np.arange(20, max_frequency + 1)
    ro_frequencies = 2 * np.pi * frequencies
    tau = mic_dist / c

    tf = 1 - np.exp(-1j * ro_frequencies * ((mic_dist/c) * np.cos(phi) + tau))

    return tf

def cardioid_tf_quantized(phi: int, mic_dist: float, max_frequency=1000, sf: int = 48000, c=343) -> np.ndarray:
    """Calculates transfer function for cardioid with quantized delays. Outuput is best used for plotting in polar plot.

    :param phi: angle of a sound source
    :type phi: int
    :param mic_dist: distance between two microphones
    :type mic_dist: float
    :param max_frequency: maximum frequency at which tf should be calculated, defaults to 1000
    :type max_frequency: int, optional
    :param sf: sampling frequency in Hz, defaults to 48000
    :type sf: int, optional
    :param c: speed of sound in m/s, defaults to 343
    :type c: int, optional
    :return: transfer function of cardioid with quantized delays
    :rtype: np.ndarray
    """
    q_step = 1/sf
    frequencies = np.arange(20, max_frequency + 1)
    ro_frequencies = 2 * np.pi * frequencies
    tau = mic_dist / c
    tau_q = round((tau / q_step)) * q_step

    ts = 1 - np.exp(-1j * ro_frequencies * ((mic_dist/c) * np.cos(phi) + tau_q))

    return ts

def find_most_similar_frequency(ratios: np.ndarray, fs: int, a: int, b: int, max_frequency: int = 1000) -> int:
    """Finds the most similar cutoff frequency of the butterworth filter
    to the given ratios of the transfer function of the cardioid microphone array. It is done by calculating the mean absolute error
    between the ratios and the magnitude of the frequency response of the butterworth filter. 

    :param ratios: Ratios of the transfer function of the cardioid microphone array.
    :type ratios: np.ndarray
    :param a: Start of the frequency range.
    :type a: int
    :param b: End of the frequency range.
    :type b: int
    :return: Most similar cutoff frequency.
    :rtype: int
    """
    n_points = b - a  + 1
    frequencies = np.linspace(a, b, n_points)
    best_frequency = a
    lowest_mae = np.inf
    
    for cutoff in frequencies:
        # Butterworth filter order
        order = 1
        # Filter design
        b, a = butter(order, cutoff / (0.5 * fs), btype='low', analog=False)
        # Frequency response
        w, h = freqz(b, a, worN=np.linspace(20, max_frequency, max_frequency - 19) * 2*np.pi / fs)
        magnitude = np.abs(h) * ratios[0]

        mean = np.mean(np.abs((ratios - magnitude[:len(ratios)])))
        if mean < lowest_mae:
            lowest_mae = mean
            best_frequency = cutoff

    return best_frequency

def approximation_radius(circle_radius: float, numenator: int, denominator: int) -> float:
    """This function calculates distance between 2 mics that are approximated. This is needed
    because they do not lay on the circle of the microphone array, therefore radius cannot be
    used.

    :param circle_radius: Radius of the microphone array
    :type circle_radius: float
    :param numenator: numerator of the fraction of the weight v
    :type numenator: int
    :param denominator: denominator of the fraction of the weight v
    :type denominator: int
    :return: Distance between 2 approximated microphones
    :rtype: float
    """
    # Distance between 2 neighbouring microphones. Calculated using law of cosines formula (c^2 = a^2 + b^2 -2*a*b**cos(gamma))
    dist_between_mics = np.sqrt(circle_radius**2 + circle_radius**2 -2*(circle_radius**2)*np.cos((2*np.pi)/8))
    my_distance = numenator/denominator * dist_between_mics
    return np.sqrt((my_distance)**2 + circle_radius**2 - 2*circle_radius*my_distance*np.cos(np.radians(67.5)))

def tau_compensation(delta_norm: float, delta_approx: float, sf: int = 48000, quantize_tau: bool = True, c: int = 343) -> float:
    """This function calculates the compensation of shorter distance between physical (delta_norm) mics and longer distance between 
    approximated mics (delta_approx). It is done by calculating heigher delay tau that should be added to the approximated delaying
    mic. It returns compensated tau for the delaying approximated mic.

    :param delta_norm: Distance between physical microphones
    :type delta_norm: float
    :param delta_approx: Distance between approximated microphones
    :type delta_approx: float
    :param sf: sampling frequency in Hz, defaults to 48000
    :type sf: int, optional
    :param quantize_tau: If True, the tau is quantized to the sampling frequency, defaults to True
    :type quantize_tau: bool, optional
    :param c: speed of sound in m/s, defaults to 343
    :type c: int, optional
    :return: Compensated tau for the delaying approximated mic
    :rtype: float
    """
    q_step = 1/sf
    tau_norm = delta_norm/c
    if quantize_tau:
        tau_norm = round(tau_norm / q_step) * q_step
    tau_approx = (delta_approx * tau_norm) / delta_norm
    difference = tau_norm - tau_approx
    tau_approx_compensated = difference + tau_norm
    return tau_approx_compensated
