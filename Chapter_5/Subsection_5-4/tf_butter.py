"""
These graphs show how much the loudness from the "imaginary" microphones calculated 
by averaging the surrounding "physical" ones. The transfer functions show that the loudness
cardioid calculated from the "imaginary" microphones is below 1 kHz at key frequencies 
than the cardioid from the "physical microphones". This implies that in order to find the loudest
source using the cardioids, we need to amplify the "imaginary" microphones adequately to get this
to eliminate this undesirable effect.

At the same time, the transfer functions are also affected by quantization to integer sample counts. This then needs to be 
It is necessary to install the PyQt5 package, using pip can be installed in this way:

pip install PyQt5
""" 
import sys
import os
import numpy as np
from scipy.signal import butter, freqz

# import matplotlib
# matplotlib.use('Qt5Agg') 
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
MAX_FREQUENCY = 1000
C = 343 # Speed of sound
PHI = 0 # Angle from which the sound waves reach the microphone array
BASE_MIC_DISTANCE = 0.1 # 10 cm
APPROX_MIC_DISTANCE = cf.approximation_radius(BASE_MIC_DISTANCE, 1, 2)
# ----------------------------------------------------------------------------------
# --------------- TRANSFER FUNCTION CALCULATION ---------------

# Transfer function for physical microphone with quantized tau
tf_base_q = np.abs(cf.cardioid_tf_quantized(PHI, BASE_MIC_DISTANCE, MAX_FREQUENCY))

# Transfer function for approximated microhone with quantized tau
tf_approx_q = np.abs(cf.cardioid_tf_quantized(PHI, APPROX_MIC_DISTANCE, MAX_FREQUENCY))

# --------------- AVERAGE CALCULATION ---------------
# Calculates ratios between 2 transfer functions. 
# Values greater than 1 indicates by how much is base tf stronger than approximated tf.
# Values smaller than 1 indicates by how much is base tf weaker than approximated tf.
ratios_q = np.abs(tf_base_q/tf_approx_q)

# --------------- FILTER CALCULATION ---------------
fs = 48000 
most_similar_frequency = cf.find_most_similar_frequency(ratios_q, fs, 2000, 8000, MAX_FREQUENCY)
print(f"Most similar frequency is {most_similar_frequency}")
 
cutoff = most_similar_frequency 
ORDER = 1
b, a = butter(ORDER, cutoff / (0.5 * fs), btype='low', analog=False)
freqs, response = freqz(b, a, worN=np.linspace(20, MAX_FREQUENCY, MAX_FREQUENCY - 19) * 2*np.pi / fs)
butter_magnitudes = np.abs(response) * ratios_q[0]

# --------------- DISPLAY PLOTS ---------------
frequencies = np.linspace(20, MAX_FREQUENCY, len(tf_base_q))

# Graf 1
plt.figure(2, figsize=(9, 6))
plt.semilogx(frequencies, tf_base_q, color='blue')
plt.semilogx(frequencies, tf_approx_q, color='darkred')
plt.title('Quantized Transfer Functions')
plt.xlabel('Frekvence (Hz)')
plt.ylabel('Amplituda')
plt.grid(True, which="both", ls="--")
plt.xlim(20, MAX_FREQUENCY)

# Graf 2
plt.figure(3, figsize=(9, 6))
# plt.semilogx(frequencies, ratios, color='lightcoral', label='Ratio Normal')
plt.semilogx(frequencies, ratios_q, color='darkred',)
plt.semilogx(frequencies, butter_magnitudes, color='orange')
# plt.title('Ratios and Filter Response')
plt.xlabel('Frekvence (Hz)')
plt.ylabel('Poměr mezi přenosovými funkcemi')
plt.grid(True, which="both", ls="--")
plt.xlim(20, MAX_FREQUENCY)

plt.show()