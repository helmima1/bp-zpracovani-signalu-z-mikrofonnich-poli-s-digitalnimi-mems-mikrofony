# Průběh testování

Testování bylo prováděné pro následující scénáře:

- 1) Frekvence 600 Hz
  -- 1a) Lokalizace pomocí DMA prvního řádu s kompenzací přenosové funkce aproximovaných mikrofonů pomocí butterworthova filtru
  -- 1b) Lokalizace pomocí DMA prvního řádu s kompenzací přenosové funkce aproximovaných mikrofonů pomocí lineární interpolace
  -- 1c) Lokalizace pomocí DSB
- 2) Frekvence 2000 Hz
  -- 2a) Lokalizace pomocí DSB

Pojmenování souborů v rámci měření dodržuje jmennou konvenci

frekvence_mode_dBreduction.txt,

kde mode rozlišuje buď snižování hladiny šumu nebo sinusoidy o počet decibelů definovaný v dBreduction.