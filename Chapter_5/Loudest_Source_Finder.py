import os
import sys
import sounddevice as sd
from functools import partial
import numpy as np
import multiprocessing
from multiprocessing import shared_memory

current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
from ClassArchitecture import *

# ------------------------------------------------------------
# ------------------- MODIFYING VARIABLES --------------------
# ------------------------------------------------------------
# App regime
APP_REGIME = AppRegime.DSB_CARD_LOOP 

# Audio variables
DURATION = 200
OUTPUT_DEVICE_ID = 0 # ID of the output device - use print(sd.query_devices()) to find out the ID or desired output device

# Phi resolution
PHI_STEPS = 64 # This number must be dividble by 8 for DSB_CARD regimes
THETA_STEPS = 1

# Computation process refresh time
COMPUTATION_THREAD_REFRESH_TIME = 0 # in seconds

# Analysis variables
CALCULATE_AVERAGE_TIME = True
PRINT_STATUS = True
SHOW_TIME = False
SHOW_ANGLES = True

# VISUALIZATION
SHOW_VISUALIZATION = True
VISUALIZATION_REFRESH_TIME = 0.1 # in seconds

# First-order DMA regime
DMA_COMP_REGIME = DMACompRegime.INTERPOL
RMS_FILTER = RMSfilter.LOWPASS_ONLY

# ------------------------------------------------------------
# ----------- CMA PROTOTYPE VARIABLES DEFINITIONS ------------
# ------------------------------------------------------------
MICROPHONE_PAIRS = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]] # IDs of microphones facing in front of each other
ACTIVE_CHANNELS_IDS = np.array([0, 1, 2, 3, 8, 9, 10, 11]) # Ids of active channels in this current FPGA configuration
BOUNDARY_CUTOFF_FREQUENCY = 1000

SF = 48000 # sampling frequency
BUFFER_SIZE = 4800
CHANNELS_IN = 16 # number of input channels
# ------------------------------------------------------------
# ------------------- CLASSES DEFINITIONS --------------------
# ------------------------------------------------------------
# Microphone array properties definition
microphoneArray = MicrophoneArray(radius=0.05,
                              m=8,
                              c=343,
                              sf=SF,
                              buffer_size=BUFFER_SIZE,
                              channels_in=CHANNELS_IN,
                              active_channels_ids=ACTIVE_CHANNELS_IDS
                            )

# Delay and sum beamforming properties definition
dsb = DSB(microphoneArray=microphoneArray)

sourceFinderDsb = SourceFinderDSB(dsb_instance=dsb, 
                                  theta_steps=THETA_STEPS,
                                  phi_steps=PHI_STEPS,
                                  duration=DURATION,
                                  show_angles=SHOW_ANGLES,
                                  show_time=SHOW_TIME,
                                  calculate_average_time=CALCULATE_AVERAGE_TIME,
                                  show_visualization=SHOW_VISUALIZATION,
                                  refreshing_time=COMPUTATION_THREAD_REFRESH_TIME)

sourceFinderDSBCardioid = SourceFinderDSBCardioid(dsb_instance=dsb,
                                                  theta_steps=THETA_STEPS,
                                                  phi_steps=PHI_STEPS,
                                                  mic_pairs=MICROPHONE_PAIRS,
                                                  boundary_cutoff_freq=BOUNDARY_CUTOFF_FREQUENCY,
                                                  duration=DURATION,
                                                  show_angles=SHOW_ANGLES,
                                                  show_time=SHOW_TIME,
                                                  calculate_average_time=CALCULATE_AVERAGE_TIME,
                                                  show_visualization=SHOW_VISUALIZATION,
                                                  refreshing_time=COMPUTATION_THREAD_REFRESH_TIME,
                                                  rms_filter=RMS_FILTER,
                                                  regime=DMA_COMP_REGIME)
# ------------------------------------------------------------

def callback(buffer_shm_name, buffer_shm_lock, buffer_ready_event, max_rms_indx, indata, outdata, frames, t, status):
    if status and PRINT_STATUS:
        print(status)
    global previous_buffer

    if not buffer_ready_event.is_set():
        # When direction is found, send new buffer to the worker thread so it can search for a new direction
        # buffer_ready_event indicates if the worker is ready to take next buffer. If this event is set, it means the computing thread
        # is either still doing some calculations or it is waiting to slow itself down, therfore it waits for the most up to date data.
        buffer_shm = shared_memory.SharedMemory(name=buffer_shm_name)
        buffer_for_FLS = np.ndarray(indata.shape, dtype=np.float64, buffer=buffer_shm.buf)
        with buffer_shm_lock:
            buffer_for_FLS[:] = indata
        buffer_ready_event.set()

    with max_rms_indx.get_lock():
        outdata[:, 0] = outdata[:, 1] = sourceFinderDsb.apply_dsb_delays_realtime(input_buffer=indata, previous_buffer=previous_buffer, theta_index=max_rms_indx[0], phi_index=max_rms_indx[1])

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

if __name__ == "__main__":
    # --------- Searching for microphone array device id ---------
    print(sd.query_devices())
    FPGA_DEVICE_ID = dsb.find_device_by_name("FPGA MicArrayBoard: USB Audio (hw:2,0)")
    if FPGA_DEVICE_ID == None:
        raise Exception(f"Error: Microphone array prototype from FEE CTU has not been detected.")
    else:
        print(f"device found: {FPGA_DEVICE_ID}")
    sd.default.device = FPGA_DEVICE_ID, OUTPUT_DEVICE_ID
    # ------------------------------------------------------------
    
    # Initialize shared memory for the buffer transfer
    buffer_shm = shared_memory.SharedMemory(create=True, size=np.prod((BUFFER_SIZE, CHANNELS_IN)) * np.float64().itemsize)
    buffer_shm_lock = multiprocessing.Lock()
    buffer_ready_event = multiprocessing.Event()

    # Initialize shared event for transfering rms values and strongest index to callback and GUI
    max_rms_indx = multiprocessing.Array('i', 2) # Defaultly set to [0, 0]
    # C type double is compatible with numpy.float64 since it uses same amounth of bytes to store informatons.
    rms_values = multiprocessing.Array('d', sourceFinderDsb.phi_steps) 
    rms_values_updated_event = multiprocessing.Event()

    # Selecting algorithm regime based on the APP_REGIME variable defined by user at the beginning of the script
    if APP_REGIME == AppRegime.DSB_LOOP:
        computation_process = multiprocessing.Process(target=sourceFinderDsb.loop_impl, args=(
                                            buffer_shm.name, 
                                            buffer_shm_lock, 
                                            buffer_ready_event, 
                                            max_rms_indx, 
                                            rms_values, 
                                            rms_values_updated_event))
    elif APP_REGIME == AppRegime.DSB_POOL:
        computation_process = multiprocessing.Process(target=sourceFinderDsb.pool_impl, args=(
                                            buffer_shm.name, 
                                            buffer_shm_lock, 
                                            buffer_ready_event, 
                                            max_rms_indx, 
                                            rms_values, 
                                            rms_values_updated_event))
    elif APP_REGIME == AppRegime.DSB_CARD_LOOP:
        computation_process = multiprocessing.Process(target=sourceFinderDSBCardioid.loop_impl, args=(
                                            buffer_shm.name, 
                                            buffer_shm_lock, 
                                            buffer_ready_event, 
                                            max_rms_indx, 
                                            rms_values, 
                                            rms_values_updated_event))
    elif APP_REGIME == AppRegime.DSB_CARD_POOL:
        computation_process = multiprocessing.Process(target=sourceFinderDSBCardioid.pool_impl, args=(
                                            buffer_shm.name, 
                                            buffer_shm_lock, 
                                            buffer_ready_event, 
                                            max_rms_indx, 
                                            rms_values, 
                                            rms_values_updated_event))
    # Start computation process
    computation_process.start()

    # VISUALISATION PROCESS
    if SHOW_VISUALIZATION:
        realtime_visualization_process = multiprocessing.Process(target=sourceFinderDsb.realtime_visualization, args=(rms_values, rms_values_updated_event, DURATION, VISUALIZATION_REFRESH_TIME))
        realtime_visualization_process.start()

    wrapped_callback = partial(callback, buffer_shm.name, buffer_shm_lock, buffer_ready_event, max_rms_indx) # Wrapped callback function with shared memory and lock
    previous_buffer = np.zeros((BUFFER_SIZE, CHANNELS_IN)) # Setting previous buffer
    
    # Opening audio stream
    with sd.Stream(samplerate=SF, channels=[16, 2], callback=wrapped_callback, blocksize=BUFFER_SIZE):
        sd.sleep(int(DURATION * 1000))
    
    # Closing processes
    computation_process.join()
    if SHOW_VISUALIZATION:
        realtime_visualization_process.join()

    # Cleaning up shared memory to prevent memory leaks
    buffer_shm.close()
    buffer_shm.unlink()
    print("Finished")