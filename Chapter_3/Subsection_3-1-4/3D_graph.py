import os
import sys
import numpy as np
import matplotlib.pyplot as plt

# Change directories to import ComputationFunctions
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

C = 343
PHI = 180
THETA = 90
M = 8
RADIUS = 0.05
RESOLUTION = 3000
MIN_FREQUENCY = 20
MAX_FREQUENCY = 8000
# ------------------ GRAPH CALCULATIONS ------------------
frequencies_to_display = np.arange(MIN_FREQUENCY, MAX_FREQUENCY, 5)
radians_reference = np.linspace(0, 2 * np.pi, RESOLUTION)

X, Y = np.meshgrid(radians_reference, frequencies_to_display)
Z = np.empty((len(frequencies_to_display), RESOLUTION))
for frequency in range(0, len(frequencies_to_display)):
    beampattern = cf.cma_beampattern(THETA, PHI, frequencies_to_display[frequency], M, RADIUS, C, RESOLUTION)
    beampattern_amplitudes = cf.signal_to_decibels(np.abs(beampattern))
    Z[frequency] = beampattern_amplitudes
    if frequencies_to_display[frequency] % 100 == 0:
        print("Calculated already till "+str(frequencies_to_display[frequency])+" Hz")

ax = plt.axes(projection="3d")
ax.plot_surface(X, Y, Z, cmap='winter', rstride=10, cstride=10, linewidth=0)
ax.set_box_aspect([1, 3, 1])
ax.set_title(f"CMA directivity pattern at radius {RADIUS} meters", weight='bold')
ax.set_xlabel("Degrees [radians]")
ax.set_ylabel("Frequency [Hz]")
ax.set_zlabel("Amplitude [dB]")

# # Defaultní rozsahy os
# default_xlim = ax.get_xlim()
# default_ylim = ax.get_ylim()
# default_zlim = ax.get_zlim()
#
# print("Defaultní rozsah os X:", default_xlim)
# print("Defaultní rozsah os Y:", default_ylim)
# print("Defaultní rozsah os Z:", default_zlim)
#
plt.show()
print("FINISHED")