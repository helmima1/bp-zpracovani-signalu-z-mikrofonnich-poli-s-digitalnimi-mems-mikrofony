import sys
import os
import numpy as np

import matplotlib
# matplotlib.use('Qt5Agg') 
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

MAX_FREQUENCY = 1000
C = 343 # Speed of sound
PHI = 0 # Angle from which the sound waves reach the microphone array
BASE_MIC_DISTANCE = 0.1 # for physical microphones in meters
APPROX_MIC_DISTANCE = cf.approximation_radius(BASE_MIC_DISTANCE, 1, 2) # for approximated microphones in meters

tf_base = np.abs(cf.cardioid_tf(PHI, BASE_MIC_DISTANCE, MAX_FREQUENCY))
tf_approx = np.abs(cf.cardioid_tf(PHI, APPROX_MIC_DISTANCE, MAX_FREQUENCY))

# --------------- DISPLAY PLOT ---------------
frequencies = np.linspace(20, MAX_FREQUENCY, len(tf_base))

# Graf 1
plt.figure(1, figsize=(6, 4))
plt.semilogx(frequencies, tf_base, color='cornflowerblue', label='Přenosová funkce fyzických mikrofonů')
plt.semilogx(frequencies, tf_approx, color='lightcoral', label='Přenosová funkce aproximovaných mikrofonů')
# plt.title('Normal Transfer Functions')
plt.xlabel('Frekvence (Hz)')
plt.ylabel('Amplituda')
plt.grid(True, which="both", ls="--")
plt.xlim(20, MAX_FREQUENCY)
plt.legend()
plt.show()
