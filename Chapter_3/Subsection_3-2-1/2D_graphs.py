import sys
import os
import numpy as np
import matplotlib.pyplot as plt

# Change directories to import ComputationFunctions
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
FREQUENCIES = [2000, 4000, 6000]  # Frequencies used to calculate 3 graphs at the time
C = 343
THETA = 90
M = 8
RADIUS = 0.05
RESOLUTION = 2000

# --------------- BEAMPATTERN CALCULATION ---------------
beampatterns = []

for freq in FREQUENCIES:
    beampattern_normal = cf.cma_beampattern(THETA, 0, freq, M, RADIUS, C, RESOLUTION)
    amplitudes_normal = cf.signal_to_decibels(np.abs(beampattern_normal))

    beampattern_30 = cf.cma_beampattern(THETA, 30, freq, M, RADIUS, C, RESOLUTION)
    amplitudes_30 = cf.signal_to_decibels(np.abs(beampattern_30))

    beampatterns.append((amplitudes_normal, amplitudes_30))

# --------------- DISPLAYING PLOTS ---------------

fig, axes = plt.subplots(ncols=3, subplot_kw={'projection': 'polar'}, figsize=(10.3, 3), dpi=200)
reference_radians_values = np.linspace(0, 2 * np.pi, RESOLUTION)
for cols in range(0, 3):
    axes[cols].plot(reference_radians_values, beampatterns[cols][1], color='#96BAFF', linewidth='1.2')
    axes[cols].plot(reference_radians_values, beampatterns[cols][0], color='green', linewidth='1.2')
    axes[cols].set_ylim([-40, 0])
    axes[cols].set_title(f"{FREQUENCIES[cols]} Hz", va='bottom')
    box = axes[cols].get_position()
    axes[cols].set_position([box.x0, box.y0, box.width * 0.9, box.height * 0.9])
    axes[cols].set_yticks([-40, -30, -20, -10, 0])

plt.show()