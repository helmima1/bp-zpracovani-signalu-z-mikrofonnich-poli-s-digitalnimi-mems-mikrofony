import sys
import os
import numpy as np

import matplotlib
# matplotlib.use('Qt5Agg') 
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- CONSTANTS DECLARATIONS ---------------
MAX_FREQUENCY = 20000
C = 343
PHI = 0
MIC_DIST = 0.1 # For angle 0

# --------------- TRANSFER FUNCTION CALCULATION ---------------
tf = cf.cardioid_tf_quantized(phi=PHI, mic_dist=MIC_DIST, max_frequency=MAX_FREQUENCY)
tf_amplitudes = cf.signal_to_decibels(np.abs(tf))

# --------------- PLOTTING ---------------
frequencies = np.linspace(20, MAX_FREQUENCY, len(tf_amplitudes))
plt.figure(figsize=(6, 4))
plt.semilogx(frequencies, tf_amplitudes, color='cornflowerblue')
plt.xlabel('Frekvence (Hz)')
plt.ylabel('Amplituda (dB)')
plt.grid(True, which="both", ls="--")
plt.xlim(20, MAX_FREQUENCY)
plt.ylim(-20, 5)

plt.show()
