"""
This script applies DSB algorithm to measured data by David Vágner, Jan Šedivý and David Ringsmuth. The output data
are then stored in "Processed_recordings" folder.
"""
import os
import sys
import soundfile as sf
import numpy as np
original_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(original_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

os.chdir(original_dir)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
MIC_ORDER = [3, 4, 6, 0, 2, 5, 7, 1]  # Sorting channels in the correct positions
RADIUS = 0.05  # in meters
M = 8
PHI = 0  # degrees
THETA = 90  # degrees
C = 343  # meters/second
SF = 48000  # Hz

V = 0.5 # weight for mic approximation

# -------------------------------------------------------------
# ------------------- CALCULATION OF DELAYS -------------------
# -------------------------------------------------------------
delays_seconds = cf.cma_tmi(RADIUS, M, PHI, THETA, C)
delays_samples = cf.quantize_tmi_to_samples(delays_seconds, SF)
heighest_delay = max(delays_samples)

sound_delay = (RADIUS*2)/C 
cardioid_delay = round(sound_delay*SF)

# ----------------------------------------------------------------
# ------------------- PROCESSING OF RECORDINGS -------------------
# ----------------------------------------------------------------
for i in range(0, 200, 2):
    # Load data from files
    wav_file = ""
    if (i < 10):
        wav_file = f"krok_00{i}"
    elif (i < 100):
        wav_file = f"krok_0{i}"
    else:
        wav_file = f"krok_{i}"
    data, samplerate = sf.read("measurement_2023-07-27_14-44/" + wav_file + ".wav")
    
    print(f"Processing of file {wav_file}.wav ...")

    # Rearrange microphones in correct order
    if data.shape[1] == 8:
        data[:, [0, 1, 2, 3, 4, 5, 6, 7]] = data[:, MIC_ORDER]

    # Cardioid approximated between 2 mics
    delayed_signal_approx = (1 - V)*data[:, 0] + (V)*data[:, 1]
    leading_signal_approx = (1 - V)*data[:, 4] + (V)*data[:, 5]

    delayed_signal = np.pad(delayed_signal_approx, (13, 0), 'constant', constant_values=(0, 0))
    leading_signal = np.pad(leading_signal_approx, (0, 13), 'constant', constant_values=(0, 0))
    sum_signal = leading_signal - delayed_signal

    # Save files
    sf.write(f'Processed_recordings/{wav_file}_processed.wav', sum_signal, samplerate)
    print(f"File {wav_file}.wav processed!")